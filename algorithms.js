
const watchmen = (n, m, A, B) => {
  let objMergeArray = {
    daysOfWatchmen: [],
    daysOfWatchmenFlat: []
  };
  for(let i = 0 ; i < n ; i++){
    for(let j = A[i]; j <= B[i] && j <= m; j++){
      objMergeArray.daysOfWatchmen[i] = objMergeArray.daysOfWatchmen[i] ? [
        ...objMergeArray.daysOfWatchmen[i],
        j
      ] : [j]
    }
    objMergeArray.daysOfWatchmenFlat = [
      ...objMergeArray.daysOfWatchmenFlat ,
      ...objMergeArray.daysOfWatchmen[i]
    ]
  }
  return objMergeArray.daysOfWatchmenFlat.reduce((returnArray, item) =>{
        if (returnArray.includes(item)) return returnArray
        return [...returnArray, item]
    }, []).length
}

const countMisordedPairs = a => {
  let numOfKeys = 0;
  for (var i = 0; i < a.length; i++) {
    for (var j = i+1; j < a.length; j++) {
      a[i] > a[j] && numOfKeys++;
    }
  }
  return numOfKeys;
}

const knightMinSteps = (a,b,c,d) => {

  let x = Math.abs(a - c);
  let y = Math.abs(b - d);

  if( x < y) {
    const temp = x;
    x = y;
    y = temp;
  }

  switch (true) {
    case x == 2 && y == 2:
        return 4
      break;
    case x == 1 && y == 0:
        return 3
      break;
    default:
      const d = (x+y) % 3 == 0 ? (x+y)/3 : (x+y) % 3 + Math.floor((x+y)/3);
      if(d*2 > x) return d;
      let dx = (x-d*2) % 2 != 0 ? (x-d*2) + 1 : (x-d*2);
      dx = (dx/2) % 2 == 0 ? dx/2 : (dx/2) + 1;
      return dx + d
  }

}

console.log(knightMinSteps(-5,-5, 5, 5))
console.log(watchmen(3, 10, [1,2,8], [4,5,9]))
console.log(countMisordedPairs([1,3,2,5,4]));

